# Translations template for pacman-mirrors.
# Copyright (C) 2017 ORGANIZATION
# This file is distributed under the same license as the pacman-mirrors
# project.
# 
# Translators:
# Moo, 2019-2020
msgid ""
msgstr ""
"Project-Id-Version: manjaro-pacman-mirrors\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2017-10-23 20:55+0200\n"
"PO-Revision-Date: 2020-01-18 16:07+0000\n"
"Last-Translator: Moo\n"
"Language-Team: Lithuanian (http://www.transifex.com/manjarolinux/manjaro-pacman-mirrors/language/lt/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.5.1\n"
"Language: lt\n"
"Plural-Forms: nplurals=4; plural=(n % 10 == 1 && (n % 100 > 19 || n % 100 < 11) ? 0 : (n % 10 >= 2 && n % 10 <=9) && (n % 100 > 19 || n % 100 < 11) ? 1 : n % 1 != 0 ? 2: 3);\n"

#: pacman_mirrors/constants/txt.py:29
msgid "ERROR"
msgstr "KLAIDA"

#: pacman_mirrors/constants/txt.py:30
msgid "INFO"
msgstr "INFORMACIJA"

#: pacman_mirrors/constants/txt.py:31
msgid "WARNING"
msgstr "ĮSPĖJIMAS"

#: pacman_mirrors/constants/txt.py:33
msgid "HTTPException"
msgstr ""

#: pacman_mirrors/constants/txt.py:34
msgid "TIMEOUT"
msgstr ""

#: pacman_mirrors/constants/txt.py:36
msgid "API"
msgstr "API"

#: pacman_mirrors/constants/txt.py:37
msgid "BRANCH"
msgstr ""

#: pacman_mirrors/constants/txt.py:38
msgid "COUNTRY"
msgstr "ŠALIS"

#: pacman_mirrors/constants/txt.py:39
msgid "FILE"
msgstr "FAILAS"

#: pacman_mirrors/constants/txt.py:40
msgid "METHOD"
msgstr "METODAS"

#: pacman_mirrors/constants/txt.py:41
msgid "METHODS"
msgstr "METODAI"

#: pacman_mirrors/constants/txt.py:42
msgid "MISC"
msgstr ""

#: pacman_mirrors/constants/txt.py:43
msgid "NUMBER"
msgstr ""

#: pacman_mirrors/constants/txt.py:44
msgid "PATH"
msgstr "KELIAS"

#: pacman_mirrors/constants/txt.py:45
msgid "PREFIX"
msgstr ""

#: pacman_mirrors/constants/txt.py:46
msgid "PROTO"
msgstr ""

#: pacman_mirrors/constants/txt.py:47
msgid "SECONDS"
msgstr ""

#: pacman_mirrors/constants/txt.py:48 pacman_mirrors/constants/txt.py:125
msgid "URL"
msgstr "URL"

#: pacman_mirrors/constants/txt.py:49
msgid "USAGE"
msgstr "NAUDOJIMAS"

#: pacman_mirrors/constants/txt.py:51
msgid "Return branch from configuration"
msgstr ""

#: pacman_mirrors/constants/txt.py:52
msgid "Set prefix to"
msgstr ""

#: pacman_mirrors/constants/txt.py:53
msgid "Replace protocols in configuration"
msgstr ""

#: pacman_mirrors/constants/txt.py:54
msgid "Replace branch in mirrorlist"
msgstr ""

#: pacman_mirrors/constants/txt.py:55
msgid "Replace branch in configuration"
msgstr ""

#: pacman_mirrors/constants/txt.py:56
msgid "Replace mirror url in mirrorlist"
msgstr ""

#: pacman_mirrors/constants/txt.py:57
msgid "Branch name"
msgstr ""

#: pacman_mirrors/constants/txt.py:58
msgid "Comma separated list of countries, from which mirrors will be used"
msgstr ""

#: pacman_mirrors/constants/txt.py:60
msgid "Load default mirror file"
msgstr ""

#: pacman_mirrors/constants/txt.py:61
msgid "Generate mirrorlist with a number of up-to-date mirrors. Ignores:"
msgstr ""

#: pacman_mirrors/constants/txt.py:63
msgid "Generate mirrorlist with defaults"
msgstr ""

#: pacman_mirrors/constants/txt.py:64
msgid "Get current country using geolocation"
msgstr ""

#: pacman_mirrors/constants/txt.py:65
msgid "List all available countries"
msgstr ""

#: pacman_mirrors/constants/txt.py:66
msgid "Generate custom mirrorlist"
msgstr ""

#: pacman_mirrors/constants/txt.py:67
msgid "Generation method"
msgstr ""

#: pacman_mirrors/constants/txt.py:68
msgid "Use to skip generation of mirrorlist"
msgstr ""

#: pacman_mirrors/constants/txt.py:69
msgid "Quiet mode - less verbose output"
msgstr ""

#: pacman_mirrors/constants/txt.py:70
msgid "Syncronize pacman databases"
msgstr "Sinchronizuoti pacman duomenų bazes"

#: pacman_mirrors/constants/txt.py:71
msgid "Maximum waiting time for server response"
msgstr ""

#: pacman_mirrors/constants/txt.py:72
msgid "Print the pacman-mirrors version"
msgstr ""

#: pacman_mirrors/constants/txt.py:74
msgid "Branch in config is changed"
msgstr ""

#: pacman_mirrors/constants/txt.py:75
msgid "Protocols in config is changed"
msgstr ""

#: pacman_mirrors/constants/txt.py:76
msgid "Re-branch requires a branch to work with"
msgstr ""

#: pacman_mirrors/constants/txt.py:77
msgid "Branch in mirror list is changed"
msgstr ""

#: pacman_mirrors/constants/txt.py:78
msgid "Available countries are"
msgstr "Prieinamos šalys yra"

#: pacman_mirrors/constants/txt.py:79
msgid "Could not download from"
msgstr "Nepavyko atsisiųsti iš"

#: pacman_mirrors/constants/txt.py:80
msgid "Cannot read file"
msgstr "Nepavyksta perskaityti failą"

#: pacman_mirrors/constants/txt.py:81
msgid "Cannot write file"
msgstr "Nepavyksta įrašyti failą"

#: pacman_mirrors/constants/txt.py:82
msgid "Converting custom mirror file to new format"
msgstr ""

#: pacman_mirrors/constants/txt.py:83
msgid "Custom mirror file"
msgstr ""

#: pacman_mirrors/constants/txt.py:84
msgid "Custom mirror file saved"
msgstr ""

#: pacman_mirrors/constants/txt.py:85
msgid "User generated mirror list"
msgstr ""

#: pacman_mirrors/constants/txt.py:86
msgid "Deprecated argument"
msgstr ""

#: pacman_mirrors/constants/txt.py:87
msgid "doesn't exist."
msgstr "nėra."

#: pacman_mirrors/constants/txt.py:88
msgid "Downloading mirrors from"
msgstr "Atsisiunčiamos tinklavietės iš"

#: pacman_mirrors/constants/txt.py:89
msgid "Falling back to"
msgstr ""

#: pacman_mirrors/constants/txt.py:90
msgid "Internet connection appears to be down"
msgstr "Atrodo, kad neveikia interneto ryšys"

#: pacman_mirrors/constants/txt.py:91
msgid "Mirror list is generated using random method"
msgstr ""

#: pacman_mirrors/constants/txt.py:92
msgid "is missing"
msgstr "trūksta"

#: pacman_mirrors/constants/txt.py:93
msgid "The mirror file"
msgstr "Tinklaviečių failas"

#: pacman_mirrors/constants/txt.py:94
msgid "Mirror list generated and saved to"
msgstr ""

#: pacman_mirrors/constants/txt.py:95
msgid "Generated on"
msgstr ""

#: pacman_mirrors/constants/txt.py:96
msgid "custom mirrorlist"
msgstr ""

#: pacman_mirrors/constants/txt.py:97
msgid "to reset custom mirrorlist"
msgstr ""

#: pacman_mirrors/constants/txt.py:98
msgid "default mirrorlist"
msgstr ""

#: pacman_mirrors/constants/txt.py:99
msgid "to modify mirrorlist"
msgstr ""

#: pacman_mirrors/constants/txt.py:100
msgid "Mirror ranking is not available"
msgstr ""

#: pacman_mirrors/constants/txt.py:101
msgid "Must have root privileges"
msgstr ""

#: pacman_mirrors/constants/txt.py:102
msgid "The mirrors has not changed"
msgstr "Tinklavietės nepasikeitė"

#: pacman_mirrors/constants/txt.py:103
msgid "No mirrors in selection"
msgstr ""

#: pacman_mirrors/constants/txt.py:104
msgid "Option"
msgstr "Parinktis"

#: pacman_mirrors/constants/txt.py:105
msgid "Please use"
msgstr "Naudokite"

#: pacman_mirrors/constants/txt.py:106
msgid "Querying mirrors"
msgstr "Užklausiamos tinklavietės"

#: pacman_mirrors/constants/txt.py:107
msgid "Randomizing mirror list"
msgstr ""

#: pacman_mirrors/constants/txt.py:108
msgid "To remove custom config run "
msgstr ""

#: pacman_mirrors/constants/txt.py:109
msgid "This may take some time"
msgstr "Tai gali šiek tiek užtrukti"

#: pacman_mirrors/constants/txt.py:110
msgid "unknown country"
msgstr "nežinoma šalis"

#: pacman_mirrors/constants/txt.py:111
msgid "Use 0 for all mirrors"
msgstr "Naudoti 0 visoms tinklavietėms"

#: pacman_mirrors/constants/txt.py:112
msgid "Using all mirrors"
msgstr "Naudojamos visos tinklavietės"

#: pacman_mirrors/constants/txt.py:113
msgid "Using custom mirror file"
msgstr "Naudojamas tinkintų tinklaviečių failas"

#: pacman_mirrors/constants/txt.py:114
msgid "Using default mirror file"
msgstr "Naudojamas numatytųjų tinklaviečių failas"

#: pacman_mirrors/constants/txt.py:115
msgid "Writing mirror list"
msgstr "Rašomas tinklaviečių failas"

#: pacman_mirrors/constants/txt.py:118
msgid "Manjaro mirrors by response time"
msgstr "Manjaro tinklavietės pagal atsako laiką"

#: pacman_mirrors/constants/txt.py:119
msgid "Manjaro mirrors in random order"
msgstr "Manjaro tinklavietės atsitiktine tvarka"

#: pacman_mirrors/constants/txt.py:120
msgid "Check mirrors for your personal list"
msgstr ""

#: pacman_mirrors/constants/txt.py:121
msgid "Use"
msgstr ""

#: pacman_mirrors/constants/txt.py:122
msgid "Country"
msgstr "Šalis"

#: pacman_mirrors/constants/txt.py:123
msgid "Resp"
msgstr ""

#: pacman_mirrors/constants/txt.py:124
msgid "Sync"
msgstr ""

#: pacman_mirrors/constants/txt.py:126
msgid "Cancel"
msgstr "Atsisakyti"

#: pacman_mirrors/constants/txt.py:127
msgid "OK"
msgstr "Gerai"

#: pacman_mirrors/constants/txt.py:128
msgid "Confirm selections"
msgstr ""

#: pacman_mirrors/constants/txt.py:129
msgid "I want to use these mirrors"
msgstr "Aš noriu naudoti šias tinklavietes"
